$(document).ready(function() {
    var currentImage = 1;
  
    setInterval(function() {
      if (currentImage === 1) {
        $('#image1').fadeOut(400, function() {
          $(this).attr('src', 'image2.jpg').fadeIn(400);
        });
        currentImage = 2;
      } else {
        $('#image2').fadeOut(400, function() {
          $(this).attr('src', 'image1.jpg').fadeIn(400);
        });
        currentImage = 1;
      }
    }, 3000); // Change image every 3 seconds
  });